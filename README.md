# doasedit

Doasedit is a shellscript to edit files with root privileges via doas while minimizing doas usage. To accomplish this, files are first copied to /tmp and made user-writable. The editor is then run in user context to edit this file. If the content was altered the changes are written back to the original file via a doas command after the editor was closed.

## Acknowledgment

This software is a fork of [doasedit](https://github.com/AN3223/scripts/blob/master/doasedit) by Ethan R with additional inspiration from [sudoedit](https://github.com/sudo-project/sudo) and also some from another [doasedit](https://gitlab.com/magnustesshu/doasedit) by Magnus Anderson.
